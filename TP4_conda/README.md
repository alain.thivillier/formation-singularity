# R Singularity container
## R_base Version: 3.6.X   

## 1) What is conda?
Conda is an open source package management system and environment management system that runs on Windows, macOS and Linux. Conda quickly installs, runs and updates packages and their dependencies. Conda easily creates, saves, loads and switches between environments on your local computer. It was created for Python programs, but it can package and distribute software for any language.

Conda as a package manager helps you find and install packages. If you need a package that requires a different version of Python, you do not need to switch to a different environment manager, because conda is also an environment manager. With just a few commands, you can set up a totally separate environment to run that different version of Python, while continuing to run your usual version of Python in your normal environment.

## 2) Why use conda instead of apt?
It is REPRODUCTIBLE ! If you install R with this command =>
```bash
apt install -y r-base
```
Today, the version will be 4.0.4 but what about a year from now? 5 years? 
The answer is simple, you CAN'T guarantee that your program working with R 4.0.4 will run in a 6.X R version.

That is why we use conda, conda allow us to pull a version that will stay the same forever.
Example =>
```bash
conda install r-base=3.6.3
```
Will always install R 3.6.3 !
That is why we strongly recommend using conda as much as possible.

## 3) Example with R and R MKL both in 3.6 and 3.6.3
In this TP, we will first explore conda with an interactive container then we will make a "conda-R.def" file to run it =>
```bash
# distribution based on: ubuntu 18.04
Bootstrap:docker
From:ubuntu:18.04

# container pashmelba
# Build:
# sudo singularity build --sandbox conda-R.sif conda-R.def

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#COPYRIGHT INRAe 2022
#AUTHOR Jérémy Verrier
#VERSION 1
#LICENSE None
#DATE_MODIF 08/03/2022


%help
Container for TP4 CONDA

Usage:
    singularity exec python ...


%runscript
    exec "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    apt upgrade -y
    apt install -y wget 
```
Then we will build and enter in the container with a shell =>
```bash
sudo singularity build --sandbox conda-R.sif conda-R.def
singularity shell --writable conda-R.sif
```

To install conda, we have to download an archive and install it like this =>
```bash
#miniconda3: get miniconda3 version 4.7.12
wget https://repo.continuum.io/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O miniconda.sh

#install conda
bash miniconda.sh -b -p /opt/miniconda
export PATH="/opt/minicond/bin:$PATH"
```

Now, we should be able to use conda like this =>
```bash
conda search r-base
Loading channels: done
# Name                       Version           Build  Channel
r-base                         3.1.2               0  pkgs/r
r-base                         3.1.3               0  pkgs/r
r-base                         3.1.3               1  pkgs/r
r-base                         3.1.3               2  pkgs/r
r-base                         3.2.0               0  pkgs/r
r-base                         3.2.1               0  pkgs/r
r-base                         3.2.2               0  pkgs/r
r-base                         3.3.1               1  pkgs/r
r-base                         3.3.1               2  pkgs/r
r-base                         3.3.1               3  pkgs/r
r-base                         3.3.1               5  pkgs/r
r-base                         3.3.1               6  pkgs/r
r-base                         3.3.2               0  pkgs/r
r-base                         3.3.2               1  pkgs/r
r-base                         3.4.1               0  pkgs/r
r-base                         3.4.1               1  pkgs/r
r-base                         3.4.2      haf99962_0  pkgs/r
r-base                         3.4.3      h1c2f66e_4  pkgs/r
r-base                         3.4.3      h1e0a451_2  pkgs/r
r-base                         3.4.3      h290ecf8_0  pkgs/r
r-base                         3.4.3      h290ecf8_1  pkgs/r
r-base                         3.4.3      h9bb98a2_5  pkgs/r
r-base                         3.5.0      h1c2f66e_1  pkgs/r
r-base                         3.5.0      h1e0a451_1  pkgs/r
r-base                         3.5.1      h1e0a451_2  pkgs/r
r-base                         3.5.3      h067a564_0  pkgs/r
r-base                         3.5.3      h26b83e4_0  pkgs/r
r-base                         3.6.0      hce969dd_0  pkgs/r
r-base                         3.6.1      h9bb98a2_1  pkgs/r
r-base                         3.6.1      haffb61f_2  pkgs/r
r-base                         3.6.1      hce969dd_0  pkgs/r
```
As you can see, the package R 3.6.3 is not available!
A quick google search bring us this https://anaconda.org/conda-forge/r-base/files?version=3.6.3&type=conda

It exists BUT it is only available via an other channel (here conda-forge), two ways to go about it =>

Add the specified channel to conda =>
```bash
#add channels
conda config --add channels conda-forge
conda install r-base=3.6.3
```

Use the specified channel once =>
```bash
conda install -c conda-forge r-base=3.6.3
```

Then type R to check if the version is correct.
You can now make that an ".def" file and also add MKL (see TP files)
## 4) Conclusion

We strongly suggest you to use eveything that will be reproductible. Think about FAIR all the time while creating a container.

Conda is a great tool that allow you to bypass the lenghty process that is the installation of a specific version of a program (withtout conda you will have to use configure make make install on ALL dependencies and the program itself for it to be reproductible)
