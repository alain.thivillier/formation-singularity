# MPI singularity container

### https://sylabs.io/guides/3.7/user-guide/mpi.html

## 1) TO BUILD AND RUN locally

a) using a sandbox from the definition file openmpi.def
```bash
sudo singularity build --sandbox openmpi.sandbox openmpi.def
```

b) we run it interactive mode in r/w
```bash
sudo singularity shell openmpi.sandbox
export OMPI_DIR=/opt/ompi
export PATH=$OMPI_DIR/bin:$PATH
export LD_LIBRARY_PATH=$OMPI_DIR/lib:$LD_LIBRARY_PATH
export MANPATH=$OMPI_DIR/share/man:$MANPATH
mpirun --allow-run-as-root /opt/mpitest
```

Ok en local !

## 2)  TO BUILD AND RUN on a cluster

Dans l'ordre :
* Vérifier les versions de MPI disponibles sur le cluster cible
```bash
module avail 2>&1 |tr " " "\r" | grep openmpi
openmpi/gcc53/1.10.7
openmpi/gcc53/4.1.1
openmpi/icc17/2.0.1
openmpi/psm2/2.0.1
openmpi/psm2/3.1.6
openmpi/psm2/gcc49/2.0.1(default)
openmpi/psm2/gcc49/3.1.6
openmpi/psm2/gcc53/2.0.1
openmpi/psm2/gcc53/3.1.6
openmpi/psm2/gcc61/2.0.1
openmpi/psm2/gcc61/3.1.6
openmpi/psm2/gcc75/3.1.6
openmpi/psm2/icc17/2.0.1
openmpi/psm2/icc17/3.1.6
openmpi/psm2/old.version
```
* En choisir une, ici openmpi/psm2/gcc75/3.1.6

* Installer la même ou la plus proche sur le container
```bash
echo "Installing Open MPI"
export OMPI_DIR=/opt/ompi
export OMPI_VERSION=3.1.6
export OMPI_URL="https://download.open-mpi.org/release/open-mpi/v3.1/openmpi-${OMPI_VERSION}.tar.bz2"
mkdir -p /opt/ompi
mkdir -p /opt
# Download
cd /opt/ompi && wget -O openmpi-$OMPI_VERSION.tar.bz2 $OMPI_URL && tar -xjf openmpi-$OMPI_VERSION.tar.bz2
# Compile and install
cd /opt/ompi/openmpi-$OMPI_VERSION && ./configure --prefix=$OMPI_DIR && make install
```

* Envoyer ce container sur le cluster sous forme d'archive (impossible de build sans les droits ROOT)
```bash
tar cvzf openmpi.tar.gz openmpi.sif
scp openmpi.tar.gz user@cluster:openmpi.tar.gz
```

* Sortir la partie "mpi" du container !!
```bash
# Ceci ne marchera pas =>
srun singularity exec openmpi.sandbox /opt/mpitest

# Il faut laisser le cluster gérer la partie MPI et non pas la délégué au container
# La commande précedente devient donc
srun mpirun singularity exec openmpi.sif /opt/mpitest
```

* Le fichier SBATCH suivant pourrait être utilisé pour lancer l'application =>
```bash
#!/bin/sh
#SBATCH --job-name=testmpi
#SBATCH -N 1
#SBATCH -n 10
#SBATCH --time=01:00:00

module load system/singularity-3.5.3
module load compiler/gcc-6.4.0
module load openmpi/psm2/gcc75/3.1.6

echo « Running on: $SLURM_NODELIST »

# ATTENTION, sortir MPI du container (executer mpirun depuis l'hôte pour la réservation des coeurs et des noeuds)

mpirun singularity exec openmpi.sif /opt/mpitest

```
