# distribution based on: ubuntu 20.04
Bootstrap:docker
From:ubuntu:20.04

# container for R version 3.6.3
# Build:
# sudo singularity build R.3.6.3_mkl.sif Singularity.R.3.6.3_mkl.def

%environment
export LC_ALL=C
export LC_NUMERIC=en_US.UTF-8
export MKL_THREADING_LAYER=GNU

#%labels
#COPYRIGHT INRAE 2022
#VERSION 1.0
#LICENSE MIT
#DATE_MODIF MYDATEMODIF

%help
Container with R and BLAS optimized MKL
Version: 3.6.3
Default runscript: R


%runscript
    #default runscript: bedops passing all arguments from cli: $@
    exec R "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    DEBIAN_FRONTEND=noninteractive apt upgrade -y
    apt install -y wget bzip2 nano

    DEBIAN_FRONTEND=noninteractive apt install -y r-base

    ## install BLASS with MKL support
    ## cf https://software.intel.com/en-us/articles/installing-intel-free-libs-and-python-apt-repo

    wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB
    apt-key add GPG-PUB-KEY-INTEL-SW-PRODUCTS-2019.PUB

    ## all products:
    #sudo wget https://apt.repos.intel.com/setup/intelproducts.list -O /etc/apt/sources.list.d/intelproducts.list
    ## just MKL
    sh -c 'echo deb https://apt.repos.intel.com/mkl all main > /etc/apt/sources.list.d/intel-mkl.list'
    ## other (TBB, DAAL, MPI, ...) listed on page

    apt update
    DEBIAN_FRONTEND=noninteractive apt install -y intel-mkl-64bit-2018.2-046 ## wants 500+mb :-/ installs to 1.8 gb :-/

    ## update alternatives
    DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/libblas.so libblas.so-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50
    DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/libblas.so.3 libblas.so.3-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50
    DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/liblapack.so liblapack.so-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50
    DEBIAN_FRONTEND=noninteractive update-alternatives --install /usr/lib/x86_64-linux-gnu/liblapack.so.3 liblapack.so.3-x86_64-linux-gnu /opt/intel/mkl/lib/intel64/libmkl_rt.so 50

    echo "/opt/intel/lib/intel64" > /etc/ld.so.conf.d/mkl.conf
    echo "/opt/intel/mkl/lib/intel64" >> /etc/ld.so.conf.d/mkl.conf
    ldconfig

    echo "MKL_THREADING_LAYER=GNU" >> /etc/environment

    R -e 'install.packages(c("DiceEval","DiceKriging","deepgp","tidyr","dplyr"), repos = "https://cloud.r-project.org")'


    # clean up
    apt autoremove --purge
    apt clean

