# singularity services (instance)

### Use singularity image as services (daemon)

https://sylabs.io/guides/3.9/user-guide/running_services.html?highlight=instance

#### Example apache2 web server

```bash
sudo singularity instance list
sudo singularity instance start apache.min.sif apache2
sudo singularity instance stop apache2
```

### We can start the services in 3 ways:
- Bind rw folders inn the sif
- use sandbox in rw
- use permanant overlay (the prefered solution)

## 1) regular sif image and bind rw folders

- Build image
```bash
sudo singularity build service_apache.sif singularity.apache.min.def
```

- create folder that will be in rw mode
```bash
mkdir -p apacherw/run/apache2
mkdir -p apacherw/run/lock/apache2
mkdir -p apacherw/var/log/apache2
```
- Start the image instance
```bash
sudo singularity instance start --bind apacherw/run/apache2:/run/apache2,apacherw/run/lock/apache2:/run/lock/apache2,apacherw/var/log/apache2:/var/log/apache2 apache.min.sif apache2
```

#### !!! We have to anticipate all the RW files/folders

## 2) build the image in a sandbox


- Build image
```bash
sudo singularity build --sandbox service_apache.sandbox singularity.apache.min.def
```

- Start the image instance
```bash
sudo singularity instance start --writable apache.min.sif apache2
```

#### !!! in order to share the container we have to convert sandbox to sif image

## 3) use permanant overlay
https://sylabs.io/guides/3.9/user-guide/persistent_overlays.html

- Build image
```bash
sudo singularity build service_apache.sif singularity.apache.min.def
```
- Create the permanant overlay folder
```bash
mkdir -p overlay_apache2
```

- Start the image instance
```bash
sudo singularity instance start --overlay overlay_apache2/ apache.min.sif apache2
```

## Access to the web page:
```bash
http://195.221.108.97:8181/
```


## Map port inside the container to ouside (host)
map the port 80 innside the container to the port 8080 to the host ([Network Virtualisation](https://sylabs.io/guides/latest/user-guide/networking.html#network-virtualization))

```bash
 sudo singularity instance start --overlay overlay_apache2/ \
    --net --network-args "portmap=8080:80/tcp" \
    apache.min.sif apache2
```

```bash
http://localhost:8080
```
